Param (
    [Parameter(
        ValueFromPipeline = $true
    )]
    [String]$OrganizationalUnit = $null
)

function Find_Old_Users ($users) {
    #[Array]$oldUsers
    $oldUsers = New-Object System.Collections.Generic.List[System.Object]

    # compare AD time to current time, and see if the difference is greater than 20 days (time is measured in LDAP time/Microsoft Epoch)
    try {
        $currentDate = (Get-Date).ToFileTime()
        foreach($user in $users){
            if (($currentDate - $user.lastLogonTimestamp) -gt 17796454761856){
                $oldUsers.Add($user)
            }
        }
    }
    catch {
        Write-Host "An unknown error has occured. Closing script"
        exit
    }

    return $oldUsers

}



############################################ "Main" ############################################

clear

Import-Module activeDirectory

Write-Host "--- Welcome to Landon's Old Account Finder ---"
Write-Host ""
Write-Host ""
Write-Host ""



# Retrieve the OU to check (if not passed in a parameter or pipe)
if ($OrganizationalUnit -eq '' -or $null){
    $OrganizationalUnit = Read-Host "Please enter the distinguished name of the OU you wish to check (this can be found in the OU's Attribute Editor)"
}


# gather ALL users in the specified OU
Write-Host "Gathering data, please wait..."
try { $users = Get-ADUser -Filter * -SearchBase $OrganizationalUnit -Properties SamAccountName, LastLogonDate, lastLogonTimestamp }
catch { 
    Write-Host "An error occurred, and the script must exit"
    #Write-Host "$Error"
    exit
}

# trim all users to only old users
$oldUsers = Find_Old_Users($users) 


# clear Powershell console
clear
Write-Host "--- The following users have not logged in for 20 days (according to the domain controller) ---"
Write-Host ""
Write-Host ""

# some users do not have logon dates recorded
$missingDateUsers = New-Object System.Collections.Generic.List[System.Object]

# write all old users
forEach ($oldUser in $oldUsers){
    if ($oldUser.LastLogonDate -eq $null) {
        $missingDateUsers.Add($oldUser)
    }
    else { Write-Host $oldUser.SamAccountName "-- last logged in --" $oldUser.LastLogonDate }
}

Write-Host ""
Write-Host ""
Write-Host ""

Write-Host "--- The following users do not have last logon data recorded (according to the domain controller) ---"

Write-Host ""
Write-Host ""

# write all users without logon data
forEach ($missingDateUser in $missingDateUsers){
    Write-Host $missingDateUser.SamAccountName "-- is missing last logon data"
}

Write-Host ""
Write-Host ""
pause